//
// build_help.sce
// Copyright (C) James Goppert 2010 <jgoppert@users.sourceforge.net>
//
// build_help.sce is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// builder_help.sce is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.
//

tbx_build_help(TOOLBOX_TITLE,get_absolute_file_path("build_help.sce"));

// vim:ts=4:sw=4
