/*
 * sci_cpp_find.cpp
 * Copyright (C) James Goppert 2010 <jgoppert@users.sourceforge.net>
 *
 * sci_cpp_find.cpp is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sci_cpp_find.cpp is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <netinet/in.h>
#include <math.h>

#include <simgear/io/sg_socket_udp.hxx>
#include "net_fdm.hxx"
#include "net_ctrls.hxx"
#include "stack-c.h"

const static float ft2m = 1./3.2808399;
const static float rad2deg = 180/M_PI;
const static float deg2rad = 1./rad2deg;

static void htond (double &x)
{
    if ( sgIsLittleEndian() )
    {
        int    *Double_Overlay;
        int     Holding_Buffer;

        Double_Overlay = (int *) &x;
        Holding_Buffer = Double_Overlay [0];

        Double_Overlay [0] = htonl (Double_Overlay [1]);
        Double_Overlay [1] = htonl (Holding_Buffer);
    }
    else
    {
        return;
    }
}

static void htonf (float &x)
{
    if ( sgIsLittleEndian() )
    {
        int    *Float_Overlay;
        int     Holding_Buffer;

        Float_Overlay = (int *) &x;
        Holding_Buffer = Float_Overlay [0];

        Float_Overlay [0] = htonl (Holding_Buffer);
    }
    else
    {
        return;
    }
}

void htonCtrls( FGNetCtrls & net)
{
    // convert to network byte order
    net.version = htonl(net.version);
    htond(net.aileron);
    htond(net.elevator);
    htond(net.rudder);
    htond(net.aileron_trim);
    htond(net.elevator_trim);
    htond(net.rudder_trim);
    htond(net.flaps);
    net.flaps_power = htonl(net.flaps_power);
    net.flap_motor_ok = htonl(net.flap_motor_ok);

    net.num_engines = htonl(net.num_engines);
    for (int i = 0; i < FGNetCtrls::FG_MAX_ENGINES; ++i )
    {
        net.master_bat[i] = htonl(net.master_bat[i]);
        net.master_alt[i] = htonl(net.master_alt[i]);
        net.magnetos[i] = htonl(net.magnetos[i]);
        net.starter_power[i] = htonl(net.starter_power[i]);
        htond(net.throttle[i]);
        htond(net.mixture[i]);
        net.fuel_pump_power[i] = htonl(net.fuel_pump_power[i]);
        htond(net.prop_advance[i]);
        htond(net.condition[i]);
        net.engine_ok[i] = htonl(net.engine_ok[i]);
        net.mag_left_ok[i] = htonl(net.mag_left_ok[i]);
        net.mag_right_ok[i] = htonl(net.mag_right_ok[i]);
        net.spark_plugs_ok[i] = htonl(net.spark_plugs_ok[i]);
        net.oil_press_status[i] = htonl(net.oil_press_status[i]);
        net.fuel_pump_ok[i] = htonl(net.fuel_pump_ok[i]);
    }

    net.num_tanks = htonl(net.num_tanks);
    for (int i = 0; i < FGNetCtrls::FG_MAX_TANKS; ++i )
    {
        net.fuel_selector[i] = htonl(net.fuel_selector[i]);
    }

    net.cross_feed = htonl(net.cross_feed);
    htond(net.brake_left);
    htond(net.brake_right);
    htond(net.copilot_brake_left);
    htond(net.copilot_brake_right);
    htond(net.brake_parking);
    net.gear_handle = htonl(net.gear_handle);
    net.master_avionics = htonl(net.master_avionics);
    htond(net.wind_speed_kt);
    htond(net.wind_dir_deg);
    htond(net.turbulence_norm);
    htond(net.temp_c);
    htond(net.press_inhg);
    htond(net.hground);
    htond(net.magvar);
    net.icing = htonl(net.icing);
    net.speedup = htonl(net.speedup);
    net.freeze = htonl(net.freeze);
}


void htonFdm( FGNetFDM & net )
{
    net.version = ntohl(net.version);

    htond(net.latitude);
    htond(net.longitude);
    htond(net.altitude);

    htonf(net.phi);
    htonf(net.theta);
	htonf(net.psi);

	htonf(net.phidot);
    htonf(net.thetadot);
    htonf(net.psidot);

    htonf(net.vcas);

    htonf(net.A_X_pilot);
    htonf(net.A_Y_pilot);
    htonf(net.A_Z_pilot);

	htonf(net.alpha);
    htonf(net.beta);
}

void output(FGNetFDM & fdmRecvBuf)
{
    printf("%10.6fN (deg) ", fdmRecvBuf.latitude*rad2deg);
    printf("%10.6fE (deg) ",fdmRecvBuf.longitude*rad2deg);
    printf("ASL (m)=% 7.1f\n", fdmRecvBuf.altitude);

    printf("phi(deg)=% 3.0f   ", fdmRecvBuf.phi*rad2deg);
    printf("theta(deg)=% 3.0f   ", fdmRecvBuf.theta*rad2deg);
    printf("psi(deg)=% 3.0f\n", fdmRecvBuf.psi*rad2deg);

    printf("phidot(deg/s)=% 3.0f   ", fdmRecvBuf.phidot*rad2deg);
    printf("thetadot(deg/s)=% 3.0f   ", fdmRecvBuf.thetadot*rad2deg);
    printf("psidot(deg/s)=% 3.0f\n", fdmRecvBuf.psidot*rad2deg);

    printf("vcas(m/s)=% 3.0f\n", fdmRecvBuf.vcas);

    printf("Abody (m/s^2) = %10.4f %10.4f %10.4f\n",fdmRecvBuf.A_X_pilot*ft2m,fdmRecvBuf.A_Y_pilot*ft2m,fdmRecvBuf.A_Z_pilot*ft2m);

	printf("alpha(deg)=% 3.0f   ", fdmRecvBuf.alpha*rad2deg);
    printf("beta(deg)=% 3.0f   ", fdmRecvBuf.beta*rad2deg);

    printf("\n");
}

void receiveFdm(const char * host, const char * port, FGNetFDM & buf)
{
	SGIOChannel *sock = new SGSocketUDP(host,port);
	sock->open(SG_IO_IN);
	sock->read( (char*)&buf, sizeof(buf) );
	htonFdm(buf);
	delete sock;
}

void sendCtrls(const char * host, const char * port, const FGNetCtrls & buf)
{
	SGIOChannel *sock = new SGSocketUDP(host,port);
	sock->open(SG_IO_OUT);
	sock->write( (char*)&buf, sizeof(buf) );
	delete sock;
}

void receiveCtrls(const char * host, const char * port, FGNetCtrls & buf)
{
	SGIOChannel *sock = new SGSocketUDP(host,port);
	sock->open(SG_IO_IN);
	sock->read( (char*)&buf, sizeof(buf) );
	htonCtrls(buf);
	delete sock;
}

extern "C"
{
    int sci_flightGearUpdate(char * fname)
    {
		// declare variables
		int l1, m1, n1, l2, m2, n2, l3, m3, n3;
		FGNetFDM fdmRecvBuf;
        FGNetCtrls ctrlsRecvBuf, ctrlsTransBuf;

		// check for correct input/output number
		CheckRhs(1,1); // only 1
		CheckLhs(1,1); // only 1

		// get second parameter and put in u
		GetRhsVar(1, MATRIX_OF_DOUBLE_DATATYPE, &m2, &n2, &l2);
		//double u[m2][n2];
		double u[m2];
		for (int i=0;i<m2;i++) u[i] = *stk(l2+i);

		//receive flight dynamic model
		receiveFdm("localhost","5500",fdmRecvBuf);

		// create output vector
		double y[18];
		y[0] = fdmRecvBuf.latitude;
		y[1] = fdmRecvBuf.longitude;
		y[2] = fdmRecvBuf.altitude;
		y[3] = fdmRecvBuf.phi;
		y[4] = fdmRecvBuf.theta;
		y[5] = fdmRecvBuf.psi;
		y[6] = fdmRecvBuf.phidot;
		y[7] = fdmRecvBuf.thetadot;
		y[8] = fdmRecvBuf.psidot;
		y[9] = fdmRecvBuf.vcas;
		y[10] = fdmRecvBuf.A_X_pilot;
		y[11] = fdmRecvBuf.A_Y_pilot;
		y[12] = fdmRecvBuf.A_Z_pilot;
		y[13] = fdmRecvBuf.alpha;
		y[14] = fdmRecvBuf.beta;
		m3 = 15;
		n3 = 1;
		CreateVar(Rhs+1,MATRIX_OF_DOUBLE_DATATYPE,&m3,&n3,&l3);
		for (int i=0;i<m3;i++) *stk(l3+i) = y[i];
		LhsVar(1) = Rhs+1;

		// output to terminal
		//output(fdmRecvBuf);

		// receive controls
		receiveCtrls("localhost","5501",ctrlsRecvBuf);

		// set controls
		ctrlsTransBuf = ctrlsRecvBuf;
        ctrlsTransBuf.aileron = u[0];
		ctrlsTransBuf.elevator = u[1];
		ctrlsTransBuf.rudder = u[2];
		ctrlsTransBuf.throttle[0] = u[3];

		// send controls
		sendCtrls("localhost","5502",ctrlsTransBuf);
        return 0;
    }

} /* extern "C" */

// vim:ts=4:sw=4
