#!/bin/bash

projectName=aerospace_toolbox

function usage {
cat << EOF
Options:
	p
		set project path as pwd
	c
		create a new class
	s
		create a new scilab .sce file
	f
		format source code
	r
		refactor
	t
		generate tags for stl, project libraries, and local code
	u
		display usage
Examples:
	Format all of the source code: 
		$0 -f
EOF
}

function setProjectPath
{
	if [ ! -e $projectPath ]
	then
		echo please run 
		echo . $0 -s 
		echo to set the project path to the pwd
		exit 0
	fi
}

if [ $# == 0 ]
then
	usage
fi

while getopts "ps:c:difrtun:p:vx" opt; do
	case ${opt} in
		p) echo setting project path to $(pwd)
			export projectPath=$(pwd)
			;;
		s) file=${OPTARG}
			setProjectPath
			echo helper: creating scilab script ${file}		
			echo first name:; read firstName
			echo last name:; read lastName
			echo email:; read email
			year=$(date +%G)
			cp ${projectPath}/templates/template.sce ${file}.sce
			sed -i "
			s/@FILE@/${file}/g ;
			s/@FIRSTNAME@/${firstName}/g ;
			s/@LASTNAME@/${lastName}/g ;
			s/@EMAIL@/${email}/g ;
			s/@YEAR@/${year}/g ;" \
			${file}.sce
			;;
		c) class=${OPTARG} 
			setProjectPath
			echo helper: creating class ${class}		
			echo first name:; read firstName
			echo last name:; read lastName
			echo email:; read email
			echo namespace:; read namespace
			year=$(date +%G)
			cp ${projectPath}/templates/template.cpp ${class}.cpp
			cp ${projectPath}/templates/template.hpp ${class}.hpp
			find . -regex ".*${class}.\(cpp\|hpp\)" \
			-exec sed -i "
			s/@CLASS@/${class}/g;
			s/@FIRSTNAME@/${firstName}/g;
			s/@LASTNAME@/${lastName}/g;
			s/@EMAIL@/${email}/g;
			s/@YEAR@/${year}/g;
			s/@NAMESPACE@/${namespace}/g;" {} \;
			;;
		f) echo helper: formatting source code
			astyle --style=ansi $(find . -regex '.*\.\(cpp\|c\|cc\|h\|hpp\)')
			rm -f $(find . -regex '.*\.orig')
			;;
		r) echo helper: refactoring
			echo -n "original name: "; read orig
			echo -n "new name: "; read new
			find . -regex '.*\.\(cpp\|c\|cc\|h\|hpp\|.am\)' \
				-exec sed -i "s|$orig|$new|g" {} \;
			;;
		t) echo autogen: generating tags
			vimrc=~/.vimrc
			tagFile="~/.vim/tags/${projectName}Tags"

			# Create the directory
			if [ -f ~/.vim/tags ] 
			then
				rm ~/.vim/tags
			fi
			mkdir -p ~/.vim/tags

			# Generate the tags
			ctags -RV --language-force=C++ --c++-kinds=+p --fields=+iaS \
				--extra=+q -f ~/.vim/tags/${projectName}Tags \
				/usr/include/pango* \
				/usr/include/gtk* \
				/usr/include/cairo* \
				/usr/include/boost/numeric \
				/usr/include/boost/asio \
				/usr/include/osg* \
				/usr/include/Serial* 

			# Add tags to .vimrc
			if grep -q "set tags+=$tagFile" $vimrc; then
				echo "Tags found in .vimrc"
			else
				echo "set tags+=$tagFile" >> $vimrc
				echo "I added them to .vimrc"
			fi
			;;
		u) usage
			;;
		?) usage
			;;
	esac
done

# vim:ts=4:sw=4
