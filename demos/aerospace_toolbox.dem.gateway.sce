// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("aerospace_toolbox.dem.gateway.sce");

subdemolist = ["demo flightGearUpdate"             ,"flightGearUpdate.dem.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
