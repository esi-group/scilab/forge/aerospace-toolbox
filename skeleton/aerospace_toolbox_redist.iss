;##############################################################################################################
; Inno Setup Install script for aerospace_toolbox
; http://www.jrsoftware.org/isinfo.php
; Allan CORNET
; This file is released into the public domain
;##############################################################################################################
; modify this path where is aerospace_toolbox directory
#define BinariesSourcePath "C:\Programs files\scilab-5.0\contrib\aerospace_toolbox"
;
#define aerospace_toolbox_version "0.1"
#define CurrentYear "2008"
#define aerospace_toolboxDirFilename "aerospace_toolbox"
;##############################################################################################################
[Setup]
; Debut Donn�es de base � renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=Aerospace Toolbox
AppVerName=Aerospace Toolbox version 0.1
DefaultDirName={pf}\{#aerospace_toolboxDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Your Company
BackColorDirection=lefttoright
AppCopyright=Copyright � {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#aerospace_toolbox_version}
VersionInfoCompany=Your Company
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
Source: etc\aerospace_toolbox.quit; DestDir: {app}\etc
Source: etc\aerospace_toolbox.start; DestDir: {app}\etc
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: sci_gateway\loader_gateway.sce; DestDir: {app}\sci_gateway
Source: sci_gateway\c\loader.sce; DestDir: {app}\sci_gateway\c
Source: sci_gateway\c\aerospace_c.dll; DestDir: {app}\sci_gateway\c
Source: sci_gateway\cpp\loader.sce; DestDir: {app}\sci_gateway\cpp
Source: sci_gateway\cpp\aerospace_cpp.dll; DestDir: {app}\sci_gateway\cpp
Source: sci_gateway\fortran\loader.sce; DestDir: {app}\sci_gateway\fortran
Source: sci_gateway\fortran\aerospace_fortran.dll; DestDir: {app}\sci_gateway\fortran
Source: src\c\libcsum.dll; DestDir: {app}\src\c
Source: src\c\loader.sce; DestDir: {app}\src\c
Source: src\fortran\libfsum.dll; DestDir: {app}\src\fortran
Source: src\fortran\loader.sce; DestDir: {app}\src\fortran
Source: tests\*.*; DestDir: {app}\tests; Flags: recursesubdirs
;Source: includes\*.h; DestDir: {app}\includes; Flags: recursesubdirs
;Source: locales\*.*; DestDir: {app}\locales; Flags: recursesubdirs
Source: demos\*.*; DestDir: {app}\locales; Flags: recursesubdirs
;
;##############################################################################################################
