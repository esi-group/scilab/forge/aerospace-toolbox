/*
 * sci_cpp_find.cpp
 * Copyright (C) James Goppert 2010 <jgoppert@users.sourceforge.net>
 *
 * sci_cpp_find.cpp is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * sci_cpp_find.cpp is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>

#include <stdio.h>
#include <netinet/in.h>
#include <math.h>
#include <simgear/io/sg_socket_udp.hxx>
#include "net_fdm.hxx"

const static float ft2m = 1./3.2808399;
const static float rad2deg = 180/M_PI;
const static float deg2rad = 1./rad2deg;

static void htond (double &x) {
  if ( sgIsLittleEndian() ) {
    int    *Double_Overlay;
    int     Holding_Buffer;
    
    Double_Overlay = (int *) &x;
    Holding_Buffer = Double_Overlay [0];
    
    Double_Overlay [0] = htonl (Double_Overlay [1]);
    Double_Overlay [1] = htonl (Holding_Buffer);
  } else {
    return;
  }
}

static void htonf (float &x) {
  if ( sgIsLittleEndian() ) {
    int    *Float_Overlay;
    int     Holding_Buffer;
    
    Float_Overlay = (int *) &x;
    Holding_Buffer = Float_Overlay [0];
    
    Float_Overlay [0] = htonl (Holding_Buffer);
  } else {
    return;
  }
}

int fgLoop() {

  int ret;
  SGIOChannel *fdmRecv = new SGSocketUDP("localhost","5500");
  SGIOChannel *fdmTrans = new SGSocketUDP("localhost","5501");
  fdmRecv->open(SG_IO_IN);
  fdmTrans->open(SG_IO_OUT);

  int i=0;
  while(1) {
	i++;
    FGNetFDM fdmRecvBuf, fdmTransBuf;
    ret = fdmRecv->read( (char*)&fdmRecvBuf, sizeof(fdmRecvBuf) );
	if(ret<0) {
      printf("error %i while receiving fdm\n",ret);
      return -2;      
    };
	fdmTransBuf = fdmRecvBuf;
	fdmTransBuf.rudder = 30*deg2rad*sin(i*2*M_PI/120);;
	fdmTransBuf.elevator = 30*deg2rad*sin(i*2*M_PI/120);
	fdmTransBuf.left_aileron = 30*deg2rad*sin(i*2*M_PI/120);
	fdmTransBuf.right_aileron = 30*deg2rad*sin(i*2*M_PI/120);
	fdmTransBuf.rpm[0] = 1000+1000*sin(i*2*M_PI/120);

	htonf(fdmTransBuf.rudder);
	htonf(fdmTransBuf.elevator);
	htonf(fdmTransBuf.left_aileron);
	htonf(fdmTransBuf.right_aileron);
	htonf(fdmTransBuf.rpm[0]);

	ret = fdmTrans->write( (char*)&fdmTransBuf, sizeof(fdmTransBuf) );
	if(ret<0) {
      printf("error %i while transmitting fdm\n",ret);
      return -2;      
    };

    // Geodetic Position
	htond(fdmRecvBuf.latitude);
	htond(fdmRecvBuf.longitude);
	htonf(fdmRecvBuf.agl);

	// NED velocity
  	htonf(fdmRecvBuf.v_north);
   	htonf(fdmRecvBuf.v_east);	
	htonf(fdmRecvBuf.v_down);

	// Attitude
	htonf(fdmRecvBuf.phi);
	htonf(fdmRecvBuf.theta);
	htonf(fdmRecvBuf.psi);

	// Attitude Rates
	htonf(fdmRecvBuf.phidot);
	htonf(fdmRecvBuf.psidot);
	htonf(fdmRecvBuf.thetadot);

	// Control
	htonf(fdmRecvBuf.elevator);
	htonf(fdmRecvBuf.left_aileron);
	htonf(fdmRecvBuf.right_aileron);
	htonf(fdmRecvBuf.rudder);
	htonf(fdmRecvBuf.rpm[0]);

	// Accelerometer
	htonf(fdmRecvBuf.A_X_pilot);
	htonf(fdmRecvBuf.A_Y_pilot);
	htonf(fdmRecvBuf.A_Z_pilot);

	// Output
	if (i++%12==0) // 10 hz if model running at 120 Hz
	{
		printf("%10.6fN (deg) ", fdmRecvBuf.latitude*rad2deg);
		printf("%10.6fE (deg) ",fdmRecvBuf.longitude*rad2deg);
		printf("AGL (m)=% 7.1f\n", fdmRecvBuf.agl);

		printf("vNED (m/s)=% 3.0f %3.0f %3.0f", fdmRecvBuf.v_north*ft2m, fdmRecvBuf.v_east*ft2m, fdmRecvBuf.v_down*ft2m);

		printf("phi(deg)=% 3.0f   ", fdmRecvBuf.phi*rad2deg);
		printf("theta(deg)=% 3.0f   ", fdmRecvBuf.theta*rad2deg);
		printf("psi(deg)=% 3.0f\n", fdmRecvBuf.psi*rad2deg);

		printf("phidot(deg/s)=% 3.0f   ", fdmRecvBuf.phidot*rad2deg);
		printf("thetadot(deg/s)=% 3.0f   ", fdmRecvBuf.thetadot*rad2deg);
		printf("psidot(deg/s)=% 3.0f\n", fdmRecvBuf.psidot*rad2deg);


		printf("elevator (deg)= % 3.0f ", fdmRecvBuf.elevator*rad2deg);
		printf("left aileron (deg)= % 3.0f ", fdmRecvBuf.left_aileron*rad2deg);
		printf("right aileron (deg)= % 3.0f ", fdmRecvBuf.right_aileron*rad2deg);
		printf("rudder (deg)= % 3.0f ", fdmRecvBuf.rudder*rad2deg);
		printf("rpm = %3.0f\n", fdmRecvBuf.rpm[0]);

		printf("Abody (m/s^2) = %10.4f %10.4f %10.4f\n",fdmRecvBuf.A_X_pilot*ft2m,fdmRecvBuf.A_Y_pilot*ft2m,fdmRecvBuf.A_Z_pilot*ft2m);

		printf("\n");
	}
  };
  return -1;
};

// vim:ts=4:sw=4
/* ==================================================================== */
extern "C" 
{
/* ==================================================================== */	
  #include "stack-c.h"
  #include "api_scilab.h"
  #include "Scierror.h"
  #include "MALLOC.h"
/* ==================================================================== */
  int sci_cpp_find(char *fname) 
  {
    SciErr sciErr;
    
    int m1 = 0, n1 = 0;
    int *piAddressVarOne = NULL;
    char *pStVarOne = NULL;
    int lenStVarOne = 0;
    int iType1 = 0;
    
    int m2 = 0, n2 = 0;
    int *piAddressVarTwo = NULL;
    char *pStVarTwo = NULL;
    int lenStVarTwo = 0;
    int iType2 = 0;
    
    /* Check the number of input argument */
    CheckRhs(2,2); 
    
    /* Check the number of output argument */
    CheckLhs(1,1);
    
    /* get Address of inputs */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    }     
    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    /* checks types */
    sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    if ( iType1 != sci_strings )
    {
      Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,1);
      return 0;
    }
  
    sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    if ( iType2 != sci_strings )
    {
      Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,2);
      return 0;
    }
    
    /* get strings */
    
    sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    /* check size */
    if ( (m1 != n1) && (n1 != 1) ) 
    {
      Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,1);
      return 0;
    }
    /* alloc string */
    pStVarOne = (char*)MALLOC(sizeof(char)*(lenStVarOne + 1));
    /* get string One */
    sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    if ( (m2 != n2) && (n2 != 1) ) 
    {
      Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,2);
      return 0;
    }
    /* alloc string */
    pStVarTwo = (char*)MALLOC(sizeof(char)*(lenStVarTwo + 1));
    /* get string Two */
    sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return 0;
    } 
    
    
    std::string myMessage (pStVarOne);
    if (pStVarOne) {FREE(pStVarOne); pStVarOne = NULL;}
    	
    std::string search(pStVarTwo);
    if (pStVarTwo) {FREE(pStVarTwo); pStVarTwo = NULL;}

    /* Where we will store the position */
    double dOut = 0.0;
    if (myMessage.find(search) != std::string::npos) 
    {
      /* The actual operation */
      dOut = myMessage.find(search); 
    } 
    else 
    {
      dOut = -1; /* Substring not found */
    }
    
    /* create result on stack */
    int m_out = 1, n_out = 1;
    
    createMatrixOfDouble(pvApiCtx, Rhs + 1, m_out, n_out, &dOut);
    
    LhsVar(1) = Rhs + 1; 

	// flightgear
	fgLoop();

    return 0;
	}
/* ==================================================================== */	
} /* extern "C" */
/* ==================================================================== */


// vim:ts=4:sw=4
