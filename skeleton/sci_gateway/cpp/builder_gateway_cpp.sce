// ====================================================================
// Allan CORNET
// DIGITEO 2008
// This file is released into the public domain
// ====================================================================

LIBS=[]
LDFLAGS='-lsgio'
CFLAGS='-I'+get_absolute_file_path('builder_gateway_cpp.sce')
FFLAGS=''
CC='gcc'
tbx_build_gateway('aerospace_cpp', ['cpp_find','sci_cpp_find'], ['sci_cpp_find.cxx'], ..
                  get_absolute_file_path('builder_gateway_cpp.sce'),LIBS,LDFLAGS,CFLAGS,FFLAGS,CC);

clear tbx_build_gateway;
