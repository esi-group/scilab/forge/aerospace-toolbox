// This file is released into the public domain
//=================================
// load aerospace_toolbox
if funptr('c_sum') == 0 then
  root_tlbx_path = SCI+'\contrib\aerospace_toolbox\';
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
if c_sum(3,5) <> 8 then pause,end
//=================================
